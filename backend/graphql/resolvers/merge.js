const Event = require('../../models/Event');
const User = require('../../models/User');
const { dateToString } = require('../../helpers/date')

const userFunction = async userId => {
    try {
        const user = await User.findById(userId)
        return {
            ...user._doc,
            createdEvents: eventFunction.bind(this, user._doc.createdEvents)
        }
    }
    catch (err) {
        throw err;
    }
};
const singleEventFunction = async eventId => {
    try {
        const event = await Event.findById(eventId);
        return transformEvent(event)
    }
    catch (err) {
        throw err;
    }
};
const eventFunction = async eventIds => {
    try {
        const events = await Event.find({ _id: { $in: eventIds } })
        return events.map(event => {
            return transformEvent(event)
        });
    }
    catch (err) {
        throw err;
    }
}

const transformEvent = event => {
    return {
        ...event._doc,
        date: dateToString(event._doc.date),
        creator: userFunction.bind(this, event._doc.creator)
    };
}

const transformBooking = booking => {
    return {
        ...booking._doc,
        user: userFunction.bind(this, booking._doc.user),
        event: singleEventFunction.bind(this, booking._doc.event),
        createdAt: dateToString(booking._doc.createdAt),
        updatedAt: dateToString(booking._doc.updatedAt)
    }
}
// exports.userFunction = userFunction;
// exports.eventFunction = eventFunction;
// exports.singleEventFunction = singleEventFunction;
exports.transformBooking = transformBooking;
exports.transformEvent = transformEvent;