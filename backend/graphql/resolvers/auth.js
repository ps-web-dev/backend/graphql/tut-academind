const bcrypt = require('bcryptjs');
const User = require('../../models/User');
const jwt = require('jsonwebtoken');

module.exports = {
    createUser: async args => {
        const existingUser = await User.findOne({ email: args.userInput.email })
        try {
            if (existingUser) {
                throw new Error("User exists already")
            }
            const hashedPassword = await bcrypt.hash(args.userInput.password, 12)
            const user = new User({
                email: args.userInput.email,
                password: hashedPassword
            });
            const result = await user.save();
            return { ...result._doc, password: null }
        }
        catch (err) {
            throw err;
        }
    },
    login: async ({email, password}) => {
        const user = await User.findOne({email: email});
        if(!user) {
            throw new Error("User does not exist!");
        }
        else {
            const isValid = await bcrypt.compare(password, user.password);
            if(!isValid) {
                throw new Error("Incorrect password");
            }
            else {
                const token = jwt.sign({userId: user._id, email: user.email}, 'booking', {
                    expiresIn: '1h'
                })
                return {userId: user._id, token: token, tokenExpiration: 1}
            }
        }
    },
}